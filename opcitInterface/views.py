import datetime
import json
import uuid
from importlib import import_module

import clannotation.annotator
import credcheck.cred
import httpx
from django.shortcuts import render
from django.template import Template, Context
from httpx import Response


def prefix(request):
    """
    The prefix view
    :param request: the request object
    :return: the response object
    """
    if request.POST:
        prefix_value = request.POST.get("prefix", None)
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        email = request.POST.get("email", None)

        if not prefix_value or not username or not password or not email:
            context = {
                "form_errors": "Please fill in all fields",
                "prefix": prefix_value,
                "username": username,
                "password": password,
                "email": email,
            }
            return render(
                request=request, context=context, template_name="base.html"
            )
        else:
            # first, fetch a DOI
            base_api_url = (
                f"https://api.crossref.org/prefixes/{prefix_value}/works"
            )
            response = json.loads(httpx.get(base_api_url).read())

            items = response["message"].get("items", [])

            if len(items) == 0:
                context = {
                    "form_errors": "No DOIs found for this prefix so "
                    "could not validate credentials",
                    "prefix": prefix_value,
                    "username": username,
                    "password": password,
                    "email": email,
                }
                return render(
                    request=request,
                    context=context,
                    template_name="prefix.html",
                )
            else:
                # verify that the user has permission to deposit
                doi_to_verify = items[0].get("DOI", None)

                cred = credcheck.cred.Credential(
                    username=username, password=password, doi=doi_to_verify
                )

                if not cred.is_authenticated():
                    context = {
                        "form_errors": "There was a problem with your login "
                        "credentials.",
                        "prefix": prefix_value,
                        "username": username,
                        "password": password,
                        "email": email,
                    }
                    return render(
                        request=request,
                        context=context,
                        template_name="prefix.html",
                    )
                elif not cred.is_authorised():
                    context = {
                        "form_errors": "You are not authorised to deposit to "
                        "this resource/DOI",
                        "prefix": prefix_value,
                        "username": username,
                        "password": password,
                        "email": email,
                    }
                    return render(
                        request=request,
                        context=context,
                        template_name="prefix.html",
                    )

                # create an md5 of the prefix
                md5_prefix = clannotation.annotator.Annotator.doi_to_md5(
                    prefix_value
                )

                json_object = {
                    "prefix": prefix_value,
                    "user": username,
                    "email": email,
                }

                # write the entry to S3
                from opcitInterface import settings
                import claws

                settings.Settings.AWS_CONNECTOR = claws.aws_utils.AWSConnector(
                    unsigned=False,
                    bucket=settings.Settings.BUCKET,
                    region_name="us-east-1",
                )

                settings.Settings.AWS_CONNECTOR.push_json_to_s3(
                    json_obj=json_object,
                    path=f"opcit/prefixes/{md5_prefix}.json",
                    bucket=settings.Settings.BUCKET,
                )

                context = {
                    "message": f"Activated. The crawler will process this "
                    f"prefix when it is next run. "
                }
                return render(
                    request=request,
                    context=context,
                    template_name="prefix.html",
                )

    return render(request, "prefix.html")


def index(request):
    """
    The index view
    :param request: the request object
    :return: the response object
    """
    if request.POST:
        doi = request.POST.get("doi", None)
        pdf = request.POST.get("pdf", None)
        license_field = request.POST.get("license", None)
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        email = request.POST.get("email", None)

        if (
            not doi
            or not pdf
            or not license_field
            or not username
            or not password
            or not email
        ):
            context = {
                "form_errors": "Please fill in all fields",
                "doi": doi,
                "pdf": pdf,
                "license": license_field,
                "username": username,
                "password": password,
                "email": email,
            }
            return render(
                request=request, context=context, template_name="base.html"
            )
        else:
            # check the credentials
            cred = credcheck.cred.Credential(
                username=username, password=password, doi=doi
            )

            if not cred.is_authenticated():
                context = {
                    "form_errors": "There was a problem with your login "
                    "credentials",
                    "doi": doi,
                    "pdf": pdf,
                    "license": license_field,
                    "username": username,
                    "password": password,
                    "email": email,
                }
                return render(
                    request=request, context=context, template_name="base.html"
                )
            elif not cred.is_authorised():
                context = {
                    "form_errors": "You are not authorised to deposit to this "
                    "resource/DOI",
                    "doi": doi,
                    "pdf": pdf,
                    "license": license_field,
                    "username": username,
                    "password": password,
                    "email": email,
                }
                return render(
                    request=request, context=context, template_name="base.html"
                )

            # fetch the title from the Crossref REST API
            rest_api_response = httpx.get(
                f"https://api.crossref.org/works/{doi}"
            ).read()

            try:
                rest_api_response = json.loads(rest_api_response)

                title = rest_api_response["message"]["title"][0]
                resource = rest_api_response["message"]["resource"]["primary"][
                    "URL"
                ]
            except Exception:
                context = {
                    "form_errors": "There was a problem fetching the title "
                    "from the API. Please wait until the REST "
                    "API contains your deposit.",
                    "doi": doi,
                    "pdf": pdf,
                    "license": license_field,
                    "username": username,
                    "password": password,
                    "email": email,
                }
                return render(
                    request=request, context=context, template_name="base.html"
                )

            outputs = []
            new_outputs = []

            outputs.append(resource)

            try:
                file_object = fetch_url(url=pdf)
            except Exception as e:
                message = (
                    f"Error fetching {pdf}: {e}. "
                    "This item will not be archived."
                )
                print(message)
                file_object = None

            output = None

            if file_object:
                # deposit in archives
                deposit_systems = {"Internet Archive": "ia"}
                for (
                    depositor_name,
                    depositor_module,
                ) in deposit_systems.items():
                    warnings, output = deposit(
                        depositor_name=depositor_name,
                        depositor_module=depositor_module,
                        file_object=file_object,
                        doi=doi,
                        title=title,
                    )

                    new_outputs.append(output)

            template = render_template(
                request=request,
                doi=doi,
                email=email,
                archival_outputs=new_outputs,
            )

            # build the DOI to MD5 and push the JSON to S3
            md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)
            json_response = build_resource(
                doi=doi,
                title=title,
                outputs=outputs,
                archive_outputs=new_outputs,
                license=license_field,
            )

            # push the JSON to S3
            from opcitInterface import settings

            settings.Settings.AWS_CONNECTOR.push_json_to_s3(
                json_obj=json_response,
                path=f"opcit/works/{md5_doi}.json",
                bucket=settings.Settings.BUCKET,
            )

            context = {
                "message": f"Deposit complete. The item has been archived "
                f"to {output} and multiple resolution will be "
                f"enabled if the resource goes offline."
            }
            return render(
                request=request, context=context, template_name="base.html"
            )
    return render(request, "base.html")


def build_resource(doi, title, outputs, archive_outputs, license):
    """
    Build a resource dictionary
    :param doi: the DOI
    :param title: the title
    :param outputs: the list of original outputs
    :param archive_outputs: the list of archive outputs
    :param license: the license
    :return: a dictionary
    """
    return {
        "doi": doi,
        "title": title,
        "license": license,
        "resources": outputs,
        "archive_resources": archive_outputs,
        "date": datetime.date.today().isoformat(),
    }


def deposit(
    depositor_name,
    depositor_module,
    file_object,
    doi,
    title,
) -> tuple[list, str]:
    """
    Deposit a file in an archive
    :param doi: the DOI
    :param depositor_name: the name
    :param depositor_module: depositor module string
    :param file_object: the file object to deposit
    :param title: the title
    :return: a list of warnings and an output location string
    """
    print(f"Loading module {depositor_name}")
    process_module = import_module(f"opcitInterface.{depositor_module}")

    # build a metadata dictionary
    metadata = {
        "title": title,
        "date": datetime.datetime.now().isoformat(),
        "doi": doi,
    }

    warnings = []

    archive = process_module.Archive(print)
    warnings, output = archive.deposit(
        file_object=file_object,
        warnings=warnings,
        doi=doi,
        metadata=metadata,
    )

    print(f"Deposited {doi} to {depositor_name} at {output}")

    return warnings, output


def fetch_url(url) -> bytes:
    """
    Fetch a remote URL
    :param url: the URL to fetch
    :return: the response content
    """
    with httpx.Client() as client:
        response = client.get(url)

        return response.content


def proxy_deposit(request, xml, login_id, login_passwd) -> Response:
    """
    Send a deposit to the live API
    :param request: the request object
    :param xml: the XML to deposit
    :param login_id: the login credential
    :param login_passwd: the login password
    :return: httpx Response object
    """

    url = "https://doi.crossref.org/servlet/"

    client = httpx.Client(base_url=url, timeout=30)

    url = httpx.URL(path="deposit")

    uf = {"mdFile": ("secondary_upload.xml", xml)}

    data = {
        "login_id": login_id,
        "login_passwd": login_passwd,
        "operation": "doDOICitUpload",
    }

    req = client.build_request(
        request.method,
        url,
        data=data,
        files=uf,
    )

    return client.send(req, stream=True)


def render_template(request, doi, email, archival_outputs):
    """
    Render the deposit template for a resource-only deposit
    :param request: the request object
    :param doi: the DOI
    :param email: the email
    :param archival_outputs: the list of archival outputs
    :return: the rendered template
    """
    deposit_template = (
        '<?xml version="1.0" encoding="UTF-8"?>\n'
        '<doi_batch version="4.4.2" '
        'xmlns="http://www.crossref.org/doi_resources_schema/4.4.2" '
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
        'xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd">\n'
        "<head>"
        "<doi_batch_id>{{ batch_id }}</doi_batch_id>"
        "<depositor>"
        "<depositor_name>Op Cit</depositor_name>"
        "<email_address>{{ email }}</email_address>"
        "</depositor>"
        "</head>"
        "<body>"
        "<doi_resources>"
        "<doi>{{ doi }}</doi>"
        '<collection property="list-based">'
    )

    counter = 0
    for output in archival_outputs:
        if output:
            counter = counter + 1
            deposit_template += (
                '<item label="SECONDARY_' + str(counter) + '">'
                "<resource>" + output + "</resource>"
                "</item>"
            )

    deposit_template += (
        "</collection>" "</doi_resources>" "</body>" "</doi_batch>"
    )

    deposit_template = Template(deposit_template)

    deposit_context = Context(
        {
            "doi": doi,
            "email": email,
            "batch_id": str(uuid.uuid4()),
        }
    )

    return deposit_template.render(deposit_context)
