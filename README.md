# Project Op Cit User Interface
This is a standalone interface that allows for the automatic digital preservation deposit of open-access work that has already been assigned a DOI.

![Op Cit Logo](https://gitlab.com/crossref/labs/opcit/-/raw/main/opcit/logo/logo-large.png)

![license](https://img.shields.io/gitlab/license/crossref/labs/opcit) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/opcit) <a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>

![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white) ![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) ![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

The application allows the user to specify a PDF file associated with an open-access article. It then deposits it in an extensible range of digital preservation archives and updates the original record with multiple resolution.

## Installation
The easiest install is via pip:
    
    pip install opcit

The entry point is the main deposit function, which takes a StarletteRequest object and a [longsight Instrumentation logging object](https://gitlab.com/crossref/labs/longsight).

## Background
[Project Op Cit](https://www.crossref.org/blog/a-request-for-comment-automatic-digital-preservation-and-self-healing-dois/) is _an experimental technology_ that  replaces the conventional Crossref deposit workflow with a new process that includes digital preservation. The application is designed to be called from the Crossref Labs API, and is not intended to be run as a standalone application.

The desired workflow for the application, in the long-run, is as follows:

1. The user deposits their DOI in the conventional Content System and waits for it to appear in the REST API.
2. The user visits the Op Cit landing page and, having emailed Crossref to participate, puts in the details of their DOI, Crossref login, and the PDF to preserve.
3. Op Cit stores the PDF in the Internet Archive.
4. Our content monitoring system, [Shelob](https://gitlab.com/crossref/labs/shelob), checks the PDF for content drift.
5. When the page is detected to have changed or gone offline, the page is updated to use multiple resolution.

This can be visualized as follows:

![Op Cit Workflow](images/OpCit.png)

## How this tool works

This interface tool allows for the registration of content with Op Cit. Once an item has been deposited and is available in the REST API, the user can visit the main page of this project to specify the associated PDF file and their credentials. This tool then deposits the work in the available archives and updates multiple resolution to include the preserved version.

1. Check your DOI by visiting the Crossref REST API: https://api.crossref.org/works/DOI_HERE. Ensure that this resolves to an active DOI for which you have deposit permissions.
2. Visit the main page of this project and input your DOI and the associated PDF file.
3. Press `submit`.

## Usage

Input your DOI and the associated PDF file. The tool will then deposit the work in the available archives and update multiple resolution to include the preserved version. 

## Disclaimers
This is an experimental technology without guarantee of uptime or reliability and is not intended for production use.

# Credits
* [FastAPI](https://fastapi.tiangolo.com/) for the Crossref Labs API.
* [Git](https://git-scm.com/) from Linus Torvalds _et al_.
* [.gitignore](https://github.com/github/gitignore) from Github.

&copy; Crossref 2023